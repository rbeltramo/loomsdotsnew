function ForLooms_RB5_debug6RAND_StatMovingDot_Frame3Pos_Cam_Cen_8_22_18(tag,Xcent,Ycent,loom_struct,stim_struct)

%Debug
Xcent = 0;
Ycent = 0;


% Ric 2018_08-22:
% from ContinuousEyeTrack_debug6RAND_StatMovingDot_Frame3Pos_Cam_Cen(tag,Xcent,Ycent)
% Removed UDP connection

TrialsPerCameraLOOP =50;



%%% Ric modifications:
%2017-09-02:  Integrated with continous eye tracking recording. The camera
%is reinitiated every 50 trials




%2017-18-09

%2016-10-25- Randomization is debugged: each repetition of condition is
%randomized independenly. Now the number of repetitions for LED on and off
%is the same.In RandomDEBUG


% Camera Trigger to stop the trial is sent after the visual stimulus



%2016-09-12- Randomization is debugged: each repetition of condition is
%randomized independenly. In RandomDEBUG

%2016-05-27. Enable UDP communication with LABview for eye tracking.
%Sequence:
%1) Vstim sends a UDP signal to the Eyetracker PC to initialize the
%camera(PORT 9090, "StartOneTrial") 
%2) Vstim receives UDP signal ("presentVS") to start the trial
%3) Vstim sends a TTL trigger signal o Eyetracker PC to start Ephys and Camera
%acquisition
%4) Vstim sends a UDP signal to the Eyetracker to Stop the camera (PORT
%9090, "EndTrial")
presentationOrder = 'ordered';


%The structure trials is transposed before saving and stored into 'base' ws



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE A PARAMETER TABLE %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We mimic the table that would be passed from the gui to the trialsStruct
% function which creates a trails structure containing all the stimuli that
% will be run.



% Create a table for the radially drifting bar
StimTable = {'Screen Shade (0-255)', 128, 128, 128;...
              'Bar Shade', 0, 0, 0;...
              'Width (degs)', 2,2,2;...
              'Length (degs)', 2,2,2;...  
              'StationaryFrames (degs)', [], [], [];...  % define it below
              'Sequential',[],[],[];... % define it below: 1=Sequential, 0=RandomLocations
              'Speed (degs/sec)', 30,30, 31;... %otherwise Interl doesn-t work
              'Orientation', 270, 270, 270;...
              'Timing (delay,[],wait) (s)', 1.5, NaN, 1;...%Post:at leat 2 sec, mess with LED
              'Blank', 0, [], []; 
              'Randomize', 0, [], [];... % Does not randomize here
              'Interleave', 0, [], [];... % Does not interleave here
              'Repeats', 0, [], [];...
              'BarCenter (degs)',Xcent,Ycent, [];... % In degree X and Y. X+=temporal -=nasal ; Y+=UP ; Y-=DOWN
              'Diameter (deg)', 30, 30, 30;
              'Loom_type',[],[],[];
              'L_V',-50,[],[];
              'Chunk_order',[],[],[];...
              'Chunk_Luminance',[],[],[];...
              'Coarse_stim_struct',[],[],[];
              'Loom_struct',[],[],[];
              };
          %Monitor is 140 deg width (pixel/deg based on Y)
barStimType = 'Radially Moving Bar';
loomStimType = 'Looming Stimulus';



StationaryFrames1 = [1]; %1 frame = 16.666ms, 60 positions
StationaryFrames2 = [4]; %4frames = 66.333ms, 15positions
% StationaryFrames3 = [6]; %6frames = 100 ms, 10 positions
StationaryFrames4 = [10]; %10frames = 166.666 ms, 6 positions
% StationaryFrames5 = [20]; %20frames = 333.333 ms, 3 positions

StationaryFrames6 = [60]; %60frames = 1second, central position



barTrials = trialStruct(barStimType, StimTable);

barTrials1 = barTrials;
barTrials1.StationaryFrames = StationaryFrames1;
barTrials1.Sequential= 1;

barTrials2 = barTrials;
barTrials2.StationaryFrames = StationaryFrames1;
barTrials2.Sequential= 0;

barTrials3 = barTrials;
barTrials3.StationaryFrames = StationaryFrames2;
barTrials3.Sequential= 1;

barTrials4 = barTrials;
barTrials4.StationaryFrames = StationaryFrames2;
barTrials4.Sequential= 0;

% barTrials5 = barTrials;
% barTrials5.StationaryFrames = StationaryFrames3;
% barTrials5.Sequential= 1;
% 
% barTrials6 = barTrials;
% barTrials6.StationaryFrames = StationaryFrames3;
% barTrials6.Sequential= 0;


barTrials7 = barTrials;
barTrials7.StationaryFrames = StationaryFrames4;
barTrials7.Sequential= 1;

barTrials8 = barTrials;
barTrials8.StationaryFrames = StationaryFrames4;
barTrials8.Sequential= 0;

% barTrials9 = barTrials;
% barTrials9.StationaryFrames = StationaryFrames5;
% barTrials9.Sequential= 1;
% 
% barTrials10 = barTrials;
% barTrials10.StationaryFrames = StationaryFrames5;
% barTrials10.Sequential= 0;

barTrials11 = barTrials;
barTrials11.StationaryFrames = StationaryFrames6;
barTrials11.Sequential= 333;

barTrials12 = barTrials;
barTrials12.StationaryFrames = StationaryFrames1;
barTrials12.Sequential= 666;





barTrials = [barTrials1;barTrials2;barTrials3;barTrials4;barTrials7;barTrials8;barTrials11;barTrials12];
% 
% StaticTrials = barTrials;
% 
% for i = 1:10
%     StaticTrials(i,1).Stimulus_Type = 'Static';
%     StaticTrials(i,1).Speed = barTrials(1,1).Diameter/barTrials(1,1).Speed; % In static, the speed is the duration (diameter/speed)
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RandomDEBUG = '1'  % 1= randomize, 0= no randomize

%Interleaved is always on (See below, in RandomDebug)
OrderedRepetitions = 80 ;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%assignin('base', 'gratingTrials', gratingTrials);
%assignin('base', 'barTrials', barTrials);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%% CREATE COMBINED TRIALS STRUCTURES %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % We now will create a larger trials structure containing trials for both
 % gratings and radial bars.(Fullfi
 
 % First get the unique fields from each structure
 barFieldNames = fieldnames(barTrials);

 barTrials = orderfields(barTrials);


 
%%% Insert here the looms trials 


Loom_struct1 = 'standard';
Loom_struct2 = 'chunky';
Loom_struct3 = 'spatial';

loomTrials = trialStruct(loomStimType, StimTable);

loomTrials1 = loomTrials;
loomTrials1.Loom_type = Loom_struct1;
loomTrials1.StationaryFrames = StationaryFrames1;
loomTrials1.Sequential= 0;

loomTrials2 = loomTrials;
loomTrials2.Loom_type = Loom_struct2;
loomTrials2.StationaryFrames = StationaryFrames1;
loomTrials2.Sequential= 0;

loomTrials3 = loomTrials;
loomTrials3.Loom_type = Loom_struct3;
loomTrials3.StationaryFrames = StationaryFrames1;
loomTrials3.Sequential= 0;

loomTrials = [loomTrials3;loomTrials2;loomTrials1];

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Combine the (dots trials ) with the looms
    trials = [loomTrials ; barTrials]; 
    
% Ric % Ordered repetitions and Randomize each repetition
    % RANDOMIZE and INTERLEAVE
    if OrderedRepetitions > 0
        
        
     %Randomize the set of trials and repeat the randomization at each repetition  
        RandTrials = trials;
        TOTtrials1 = trials;
        for i= 1:OrderedRepetitions
            
            %  % If the user has selected randomize we randomize the trials by using a
               % random permutation of the trials. The same set of trials
               % is re-randomized at every repetition
             if strcmp(RandomDEBUG,'1')
             RandTrials = trials(randperm(numel(trials)));
             end
           TOTtrials1 = [TOTtrials1 ; RandTrials];
        end
        
        
        
        
%% %          GENERATE RANDOM LOCATIONS
 
      for trial=1:numel(TOTtrials1)  
          
% Get info for this trial
               
            monitorInformation;
            degPerPix= (monitorInfo.degPerPix);

            width = TOTtrials1(trial).Width;
            widthPix = width/degPerPix; 
                        
            Diameter = TOTtrials1(trial).Diameter(1); % Length of the trajectory
            BarCenter_y = TOTtrials1(trial).BarCenter(1,2)-Diameter/2; %Place the center of the trajectory in the center of the RF
            BarCenter_x = TOTtrials1(trial).BarCenter(1,1);
            StationaryFrames = TOTtrials1(trial).StationaryFrames;
            Speed = TOTtrials1(trial).Speed(1);
            % Convert the length and width to pixel units
    
        %Monitor Area (In pix)
        visibleSizeX = round(1*monitorInfo.screenSizePixX);
        visibleSizeY = round(1*monitorInfo.screenSizePixY);
            
        % Determine the number of positions (bins equally spaced) where to draw the dots. The
        % number of position depends on the StationaryFrames and on the length of he
        % line where the dots are presented(= Diameter).
        %Monitor Frequency = 60Hz. 1frame = 16.666666ms
        
        FrameDuration= 16.6666666666666; %In ms, for 60Hz
        PositionDuration = StationaryFrames*FrameDuration/1000 % In seconds
        PositionsNumber = 1000/StationaryFrames/FrameDuration %How many positions to cover 1 second

        %Determine the positions
        %Use linespace to include the begining and the end of the
        %trajectory and divide the rest in equal parts
        PositionsY = (linspace(BarCenter_y,BarCenter_y+Diameter,PositionsNumber))';
        Positions=[BarCenter_x,BarCenter_y];
        for i=1:1:size(PositionsY)
            Positions(i,1)=BarCenter_x;
            Positions(i,2)=PositionsY(i,1);
        end
         
        
        TOTtrials1(trial).Positions = Positions;
        TOTtrials1(trial).PositionDuration = PositionDuration;
         
        %When the stimuli are not sequential, permute the indexes of the
        %locations
        if TOTtrials1(trial).Sequential == 0   
           TOTtrials1(trial).Positions(:,2) = Shuffle(Positions(:,2));
        end %Switch loop
        
        if TOTtrials1(trial).Sequential == 333 %This is the case with only one dot   
           TOTtrials1(trial).Positions = [BarCenter_x,BarCenter_y+Diameter/2]; %Place the only dot in the center of the trajectory
        end

        if TOTtrials1(trial).Sequential == 666 %This is the case with opposite direction   
           TOTtrials1(trial).Positions(:,2) =  wrev(Positions(:,2));  %Invert the order of the vector
        end  
        
      end % Forloop for trials    
      
 % Create TOTtrials2 randomizing within the set of trials 
 
   
        OneSetOfTrials = numel(trials);
        TOTtrials2 =TOTtrials1;
        permuteVector = 0:1:OneSetOfTrials-1; % Contains the indexes of a set of trials
        
        for t= 1:OneSetOfTrials:numel(TOTtrials1) % counter of sets of trials
            for c= 1:1:numel(permuteVector) %counter of within permuteVector(c is a vector of progressive
            %number. The random number are found in permuteVector, that is
            %shuffled each set of trials
            TOTtrials2(t+c-1) =  TOTtrials1(t+permuteVector(c));   
            end
            permuteVector = Shuffle(permuteVector);
        end
            
  
 
 
     % now perform interleave using concatenation and reshape
        % get the number of columns
        nColumns = size(TOTtrials1,2);
        % concatenate the two blocks columnwise then rotate (this
        % interleaves)
        rotated_trial_arrays = [TOTtrials1,TOTtrials2]';
        trial_arrays = reshape(rotated_trial_arrays(:),nColumns,[])';        
    end  
    trials = trial_arrays;


    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %% LOOMS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%JB-Create some variables for the exponential looms. Loom_50 is the smooth
%stimulus with an l/v = 50; coarse_50 is the same with the stimulus split
%into chunks. I use loom_struct and stim_struct throughout the actual
%stimulus presentation code so that you can have stimuli with multiple l/v
%values as desired.

% loom_50 = loom_object(-50, monitorInfo.screenDistcm);
% coarse_50 = stim_scrambler(loom_50,2.5);
% loom_struct = loom_50;
% stim_struct = coarse_50; JB I want to eventually load these from within
% the program, but so long as I am debugging it makes more sense to load
% them a single time outside of the program and then feed them in as variables. 

loomtail = 2; %This is the time you want the stimulus to remain on the screen after
%presentation, in seconds

new_win = true;
grey = [];
mask = [];
mask_id = 0;
%These will be important in the present setup section!

%This loads some of the settings necessary for a few prepartory operations
%below


%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This section adjusts the location of the stimuli on the screen for their
%chosen center positions
setup_string = 'spatial';%This lets you adjust for the center of the loom without
%generating any stimulus - spatial could be changed to chunky with no
%difference
present_setup_rig_8_23; %This loads the rest of the preparatory settings

    for i = 1:length(squares) %JB 6/12 this should allow you to alter the center
        %of the loom according the X and Y center inputs for chunky and
        %scrambled stimuli
        squares(1,i) = squares(1,i) + Xcent;
        squares(3,i) = squares(3,i) + Xcent;
        squares(2,i) = squares(2,i) + Ycent;
        squares(4,i) = squares(4,i) + Ycent;
    end
    for i = 1:length(loom)%JB 6/12 this should allow you to alter the center
        %of the loom according the X and Y center inputs for standard looms
        loom{1,i}(1,1) = loom{1,i}(1,1) + Xcent;
        loom{1,i}(1,2) = loom{1,i}(1,2) + Ycent;
        loom{1,i}(1,3) = loom{1,i}(1,3) + Xcent;
        loom{1,i}(1,4) = loom{1,i}(1,4) + Ycent;
        
        loom{2,i}(1,1) = loom{2,i}(1,1) + Xcent;
        loom{2,i}(1,2) = loom{2,i}(1,2) + Ycent;
        loom{2,i}(1,3) = loom{2,i}(1,3) + Xcent;
        loom{2,i}(1,4) = loom{2,i}(1,4) + Ycent;
    end

%This next bit creates the frame_array, the number of frames in a given stim which is determined by the l/v

	frame_array = 1:frames;

VBLT = zeros(1,frames);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    
    
    
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% SAVE THE TRIALS STRUCT %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Call the function trialStructSave to perform saving of the newly created
% trials structure

%Ric%  Transpose the structure trials and save into the base ws
assignin('base', 'trials', trials);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

trialStructSave(trials,'RB',tag)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% SET UP MONITOR INFO AND PREP PTB %%%%%%%%%%%%%%%%% 
% Get monitor info from monitorInformation located in RigSpecificInfo dir.
% This structure contains all the pertinent monitor information we will
% need such as screen size and appropriate conversions from pixels to
% visual degrees
monitorInformation;

%%%%%%%%%%%%%%%%%%%%% TURN OFF PTB SYSTEM CHECK REPORT %%%%%%%%%%%%%%%%%%%%
Screen('Preference', 'Verbosity',1); 
% This will suppress all but critical warning messages
% At the end of the code we will return the verbosity back to norm level 3
% please see the following page for an explanation of this function
% http://psychtoolbox.org/FaqWarningPrefs
% NOTE: as you debug your code comment this line because PTB will return
% back useful info about memory usage that will tell you about leaks that
% may casue problems

% When Screen('OpenWindow',w,color) is called, PTB performs many checks of
% your system. The time it takes to perform these checks depends on the
% noisiness of your system (up to two seconds on 2-photon rig). During this
% time it displays a white screen which is obviously not good for visual
% stimulation. We can disable the startup screen using the following. The
% sreen will now be black before visual stimulus
Screen('Preference', 'VisualDebuglevel', 3);
% see http://psychtoolbox.org/FaqBlueScreen for a reference

%%%%%%%%%%%%%%%%%%%%% OPEN A SCREEN & DETERMINE PARAMETERS %%%%%%%%%%%%%%%%
% Use a try except block to prevent the screen from hanging. During testing
% if the screen does hang press cntrl C or cntrl-alt del to bring up the
% task manager to stop PTB execution
try
    
    % Require OPENGL becasue some of the functions used here need the
    % OPENGL version of PTB
    AssertOpenGL;
    
%%%%%%%%%%%%%%%%%%%%%% GET SPECIFIC MONITOR INFORMATION %%%%%%%%%%%%%%%%%%%

    % SCREEN WE WILL DISPLAY ON
    %Query monitorInformation for screenNumber
    screenNumber = monitorInfo.screenNumber;

    % COLOR INFORMATION OF SCREEN
    % Get black, white and gray color values for the current monitor
    whitePix = WhiteIndex(screenNumber);
    blackPix = BlackIndex(screenNumber);
    
    %Convert balck and white to luminance values to determine gray
    %luminance
    whiteLum = PixToLum(whitePix);
    blackLum = PixToLum(blackPix);
    grayLum = (whiteLum + blackLum)/2;
    
    % Now determine the pixel value of gray from the gray luminance
    grayPix = GammaCorrect(grayLum);
    clut = GammaCorrect(1:.52549:134); %JB - This is specific to our rig and may need 
    %to be regenerated for the visual stimulation rig
   

    % CONVERSION FROM DEGS TO PX AND SIZING INFO FOR SCREEN
    %conversion factor specific to monitor
    degPerPix = monitorInfo.degPerPix_basedOnY;%BasedonY
%%%%%%%%%%%%%%%%%%%%%%%%%% INITIAL SCREEN DRAW %%%%%%%%%%%%%%%%%%%%%%%%%%%    
% We start with a gray screen before generating our stimulus and displaying
% our stimulus. 

    % HIDE CURSOR FROM SCREEN
    HideCursor;
    % OPEN A SCREEN WITH A BG COLOR OF GRAY (RETURN POINTER W)
	[w, screenRect]=Screen('OpenWindow',screenNumber, grayPix);
    
        
    % ENABLE ALPHA BLENDING OF GRATING WITH THE MASK
    Screen('BlendFunction', w, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    
%%%%%%%%%%%%%%%%%%%%%%%%% PREP SCREEN FOR DRAWING %%%%%%%%%%%%%%%%%%%%%%%%%

% SCRIPT PRIORITY LEVEL
% Query for the maximum priority level availbale on this system. This
% determines the priority level of the matlab thread (0= normal,
% 1=high, 2=realTime priority) note that a setting of 2 may cause the
% keyboard to be unresponsive. You may want to play with this number if
% you have trouble recovering the screen back
    
    priorityLevel=MaxPriority(w);
    Priority(priorityLevel);

% INTERFRAME INTERVAL INFO   
    % Get the montior inter-frame-interval 
    ifi = Screen('GetFlipInterval',w);
    
    %on old slow machines we may not be able to update every ifi. If your
    %graphics processor is too slow you can buy a better one or adjust the
    %number of frames to wait between flips below
    
    waitframes = 1; %I expect most new computers can handle updates at ifi
    ifiDuration = waitframes*ifi;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%%%%%%%%%%%%%%%%%%%%% CONSTRUCT AND DRAW TEXTURES %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is the main body of the code. We will loop through the trials
% strcuture, determine the stimulus type, construct the stimulus and
% execute the drawing in a while loop. All of this must be done in
% a single loop becasue we need to close the textures in the trial loop
% after using each texture becasue otherwise they will hang around in
% memory and cause the familiar Java runtime error: Out of memory.






%% Send via UDP the trigger to initialize the ContinuousEyeTracking_Rec :

% Set up UDP connection: port 9089 for ContinuousEyeTracking_Rec

udp_InitializeLoops=pnet('udpsocket',9089); %Creates a UDP socket and binds it to an UDP port
if udp_InitializeLoops<0 % reinitiate udp
    pnet(udp_InitializeLoops,'close');
    udp_InitializeLoops=pnet('udpsocket',9089);
end

pnet(udp_InitializeLoops,'udpconnect','169.230.189.114',9089); %Connect a destination host and port to the the UDP socket
  
message = 'StartContinuousRec' ;   

pnet(udp_InitializeLoops,'write',message); % Write message in socket
pnet(udp_InitializeLoops,'writepacket'); %Sends contents of the sockets write buffer as a UDP packet 
pnet('closeall');   

% Set the LoopCounter to 0 (For the eye tracking)
LoopCounter = 0;








% Exit code
    % This is a flag indicating we need to break from the trials
    % structure loop below. The flag becomes true (=1) if the
    % user presses any key
    exitLoop=0;
    
%% MAIN LOOP OVER TRIALS TO CONSTRUCT TEXTURES AND DRAW THEM
        for trial=1:numel(trials) 
        if exitLoop==1;
            break;
        end
   LoopCounter = LoopCounter + 1; % Each Trial add +1 to the LoopCounter
   %Reset the LoopCounter at the end when the camera is stopped.

%% Send via UDP the trigger to initialize the camera: Initialize the LOOP
if LoopCounter == 1;

% Set up UDP connection: port 9090 for EyeTracker PC
udp_InitializeCam=pnet('udpsocket',9090); %Creates a UDP socket and binds it to an UDP port
if udp_InitializeCam<0 % reinitiate udp
    pnet(udp_InitializeCam,'close');
    udp_InitializeCam=pnet('udpsocket',9090);
end

pnet(udp_InitializeCam,'udpconnect','169.230.189.114',9090); %Connect a destination host and port to the the UDP socket
  
message = 'StartOneTrial' ;   

pnet(udp_InitializeCam,'write',message); % Write message in socket
pnet(udp_InitializeCam,'writepacket'); %Sends contents of the sockets write buffer as a UDP packet 
pnet('closeall');

% %% Wait for UDP "presentVS" to start the trial
% 
% %  Creates a UDP socket and binds it to an UDP port     
% udp=pnet('udpsocket',9091);
% 
% if udp<0 % reinitiate udp
% pnet('closeall');
% udp=pnet('udpsocket',9091);
% end
% 
% while 1 %Create an infinite loop
% sizeUDP=pnet(udp,'readpacket');
% 
% % exit the while loop and flag to one if user presses any key
% if KbCheck
%    exitLoop=1;
% break;
% end
% 
% %Receive the presentVS signal form EyeTracker PC and exit the while loop
%     if sizeUDP<1    % If there is nothingh in the UDP buffer present a gray screen  
%         
%     %Display a gray screen            
%     vbl=Screen('Flip', w);
%     % The first time element of the stimulus is the delay from trigger
%     % onset to stimulus onset
%     TimeGray = vbl + 0.1; % 0.1 second
%     % Display a blank screen while the vbl is less than delay time. NOTE
%     % we are going to add 0.5*ifi to the vbl to give us some headroom
%     % to take possible timing jitter or roundoff-errors into account.
%         while (vbl < TimeGray)
%         % Draw a screen with shade matched to screen shade user chose
%         Screen('FillRect', w,grayPix);
%         % update the vbl timestamp and provide headroom for jitters
%         vbl = Screen('Flip', w, vbl + (waitframes - 0.5) * ifi);
%         % exit the while loop and flag to one if user presses any key
%             if KbCheck
%             exitLoop=1;
%             break;
%             end
%         end
%       
%     else
%     info=pnet(udp,'read',sizeUDP,'char'); %Read what is in the UDP buffer
%     name=sscanf(info,'%s %.f');   %Convert in the right format to compare strings
%       if strcmp(name,'presentVS') % Start now the trials if in the buffer there is presentVS
%       break %When the presentVS is sent the while loop is broken
%       end
%     end
% 
% end   
% pnet('closeall');
   
end    %(LoopCounter) 
        
   
   
     n=0;% construct a counter to shift the grating on each redraw
        
    % OBTAIN STIMULUS TIMING
    delay = trials(trial).Timing(1);
    wait = trials(trial).Timing(3);
    
    % note the duration depends on which stimulus is being run so we access
    % it later
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% DETERMINE THE STIMULUS TYPE AND CONSTRUCT TEXTURE %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    switch trials(trial).Stimulus_Type
 

   
        %%%%%%%%%%%%%%%%%%%%% MAKE BAR TEXTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
         case {'Radially Moving Bar'}
            % To make a static bar texture of a drifting grating we need
            % the contrast, the bar length, and the bar width. For each
            % trial in our structure we will get these two variables and
            % convert them to appropraite units and then make our texture.

            Positions= trials(trial).Positions;
            Diameter    = trials(trial).Diameter; %This is where the dot moves
            StationaryFrames = trials(trial).StationaryFrames;
            PositionDuration = trials(trial).PositionDuration;

            ScreenShade = GammaCorrect(trials(trial).Screen_Shade/whitePix*...
                                (whiteLum + blackLum));
            BarShade = GammaCorrect(trials(trial).Bar_Shade/whitePix*...
                                (whiteLum + blackLum));
            % Get length and width of the bar for this trial
            length_trial = trials(trial).Length;
            width = trials(trial).Width;
            
            % Convert the length and width to pixel units
            lengthPix = length_trial/degPerPix;
            widthPix = width/degPerPix;
    
            % Size of the area to draw grating/bars to (in pix)( 120% of 
                % monitor width)
                visibleSizeX = round(1.0*monitorInfo.screenSizePixX);
                visibleSizeY = round(1.0*monitorInfo.screenSizePixY);
            
            % Our source texture will be a full screen texture with a shade
            % equal to the bar shade
            barTex{trial} = Screen('MakeTexture',w,...
                BarShade*ones(visibleSizeX));
                               
            % Get the parameters for drawing the bar to the screen (speed
            % and orien.)
            speed = trials(trial).Speed;
            Bar_orientation = trials(trial).Orientation;

        %%%%%%%%%%%%%%%%% MAKE BLANK TEXTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case 'Blank'
         % Size of the area to draw grating/bars to (in pix)( 120% of 
         % monitor width)
            visibleSizeX = round(1.0*monitorInfo.screenSizePixX);
            visibleSizeY = round(1.0*monitorInfo.screenSizePixY);            
       
    ScreenShade = GammaCorrect(trials(trial).Screen_Shade/whitePix*...
                                (whiteLum + blackLum));
          
            
            blankTex{trial} = Screen('MakeTexture', w,...
                grayPix*ones(visibleSizeX));
    end
    
 
%%%%%%%%%%%%%%%%%%%%%%% PARALLEL PORT TRIGGER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% After constructing the stimulus texture we are now ready to trigger the 
% parallel port and begin our draw to the screen. This function
% is located in the stimGen helper functions directory.

 ScreenShade = GammaCorrect(trials(trial).Screen_Shade/whitePix*...
                                (whiteLum + blackLum)); %JB - since we don't 
                            %draw any textures we will have to define
                            %screenshade outside of that section.

ParPortTrigger;
            
%%%%%%%%%%%%%%%%%%%%%%%%% DRAW DELAY SCREEN %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
% DEVELOPER NOTE: Prior versions of stimuli used the func WaitSecs to
% draw delay screens. This is a bad practice because the function sleeps
% the matlab thread making the computer unresponsive to KbCheck clicks.
% In addition PTB only guarantees the accuracy of WaitSecs to the
% millisecond scale whereas VBL timestamps described below uses
% GetSecs() a highly accurate submillisecond estimate of the system
% time. All times should be referenced to this estimate for better
% accuracy.

% We start by performing an initial screen flip using Screen, we return
% back a time called vbl. This value is a high precision time estimate
% of when the graphics card performed a buffer swap. This time is what
% all of our times will be referenced to. More details at
% http://psychtoolbox.org/FaqFlipTimestamps
    vbl=Screen('Flip', w);
    
    % The first time element of the stimulus is the delay from trigger
    % onset to stimulus onset
    delayTime = vbl + delay;
        
    % Display a blank screen while the vbl is less than delay time. NOTE
    % we are going to add 0.5*ifi to the vbl to give us some headroom
    % to take possible timing jitter or roundoff-errors into account.
    while (vbl < delayTime)
        % Draw a screen with shade matched to screen shade user chose
        Screen('FillRect', w,ScreenShade); %was grayPix
        
        % update the vbl timestamp and provide headroom for jitters
        vbl = Screen('Flip', w, vbl + (waitframes - 0.5) * ifi);
        
        % exit the while loop and flag to one if user presses any key
        if KbCheck
            exitLoop=1;
            break;
        end
    end
        
%%%%%%%%%%%%%%%%%%%%%%%%% DRAW STIMULUS TEXTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%           
setup_string = trials(trial).Loom_type;
present_setup_rig_8_23;
if ~strcmp(trials(trial).Loom_type,'standard') %JB 6/11 - this prevents standard looms from causing an error
    lum_mat = lum_mat*2;
end

%If you have a spatially scrambled stimulus, you need to shuffle it every
%single time you play it
if strcmp(trials(trial).Loom_type,'spatial')
    stim_struct.rand_squares = stim_struct.rand_squares(:,randperm(size(stim_struct.rand_squares,2)));
end

     switch trials(trial).Stimulus_Type
         
         case 'Looming Stimulus'
             
             %Save the shuffled loom and coarse struct
             trials(trial).Coarse_stim_struct = stim_struct;
             trials(trial).Loom_struct = loom_struct;
             
             if strcmp('standard',trials(trial).Loom_type)
                     
               p=1; %set the position counter to 1
%                while (p <= size(frame_array,2))
              for p = frame_array
                    n = n+1;                   
                    Screen('FillRect', w, blackPix, loom{1,p}');%, rectColor, centeredRect); Put the stimulus in the buffer        
                    Screen('Flip', w,0,0);% Draw the stimulus on the screen. The last 0 clear the buffer after the frame
                    
                    % Draw a box at the bottom right of the screen to record all 
                    % screen flips using a photodiode. Please see the file
                    % FlipCheck.m in the stimulus directory for further explanation
                    FlipCheck(w, screenRect, [whitePix, blackPix], n)
%                     p=p+1; %Update the position counter
                    % exit the while loop and flag to one if user presses any key
%                     if KbCheck
%                         exitLoop=1;
%                         break;
%                     end
               end

              
             elseif (strcmp('chunky', trials(trial).Loom_type) | strcmp('spatial', trials(trial).Loom_type))
                    
               p=1; %set the position counter to 1
               while (p <= size(frame_array,2))
                    lum_val = clut(lum_mat(:,p) + 1);
                    grey = [lum_val; lum_val; lum_val];
                    n = n+1;
                    Screen('FillRect', w, grey, squares);
                    Screen('Flip', w,0,0);
                    % Draw a box at the bottom right of the screen to record
                    % all screen flips using a photodiode. Please see the file
                    % FlipCheck.m in the stimulus directory for further
                    % explanation
                    FlipCheck(w, screenRect, [whitePix, blackPix], n)
                    p=p+1; %Update the position counter
                    % exit the while loop and flag to one if user presses any key
                    if KbCheck
                       exitLoop=1;
                       break;
                    end
               end
            end
               
             
         case  'Radially Moving Bar'
             BarCenter_xFrame= Positions(1,1);
             BarCenter_yFrame= Positions(1,2);
        
          %This put the initial x and y in the center of the screen
          x =  visibleSizeX/2  + BarCenter_xFrame/degPerPix-Diameter/2*degPerPix ;
          y =  visibleSizeY/2  + BarCenter_yFrame/degPerPix-Diameter/2*degPerPix ;
            
          % Draw a square dot 
            Dot = [0 0 widthPix widthPix];
            centeredDot = CenterRectOnPoint(Dot, x , y);
           
          p=1; %set the position counter to 1
          
       while (p <= size(Positions,1))
            n = n+1;      
                for i =1:1:StationaryFrames % Each cycle is one frame
                Screen('FillRect', w, [0 0 0], centeredDot)%, rectColor, centeredRect); Put the stimulus in the buffer        
                Screen('Flip', w,0,0);% Draw the stimulus on the screen. The last 0 clear the buffer after the frame
                % Draw a box at the bottom right of the screen to record all 
                % screen flips using a photodiode. Please see the file
                % FlipCheck.m in the stimulus directory for further explanation
                FlipCheck(w, screenRect, [whitePix, blackPix], n)
                end
       
        p=p+1; %Update the position counter
        
           %    Now update the dstRect position
        if p<=size(Positions,1) 
      BarCenter_xFrame= Positions(p,1);
      BarCenter_yFrame= Positions(p,2);
        end
        
          %This put the initial x and y in the center of the screen
        x =  visibleSizeX/2  + BarCenter_xFrame/degPerPix-Diameter/2*degPerPix ;
        y =  visibleSizeY/2  + BarCenter_yFrame/degPerPix-Diameter/2*degPerPix ;

        %Reset centeredDot
            centeredDot = CenterRectOnPoint(Dot, x , y);
          
            % exit the while loop and flag to one if user presses any key
            if KbCheck
                exitLoop=1;
                break;
            end
      end
       
        
   
        
        
        
    
        case 'Blank'
            vbl=Screen('Flip', w);
            
            % get timing of the grating
            
            duration  = trials(trial).Timing(2);
            %  duration  = trials(trial).Timing(2); Ric, changed

            
            % Set the runtime of each trial by adding duration to vbl time
            runtime = vbl + duration;
            
            while (vbl < runtime)
                
                  n = n+1; % Add to counter for flipCheck box
                  
                % Draw a gray screen
                Screen('FillRect', w,ScreenShade);
                
                % Draw a box at the bottom right of the screen to record
                % all screen flips using a photodiode. Please see the file
                % FlipCheck.m in the stimulus directory for further
                % explanation
                FlipCheck(w, screenRect, [whitePix, blackPix], n)
              
                % update the vbl timestamp and provide headroom for jitters
                vbl = Screen('Flip', w, vbl + (waitframes - 0.5) * ifi);
                % exit the while loop and flag to one if user presses  key
                if KbCheck
                    exitLoop=1;
                    break;
                end
            end           
    end
    

    
%% %%%%%%%%%%%%%%%%%%%%% DRAW INTERSTIMULUS SCREEN %%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Between trials we want to draw a blank screen for a time of wait
        
        % Flip the screen and collect the time of the flip
        vbl=Screen('Flip', w);
        
        % We will loop until delay time referenced to the flip time
        waitTime = vbl + wait;
        % 
        while (vbl < waitTime)
            % Draw a gray screen
            Screen('FillRect', w, ScreenShade);
            
            % update the vbl timestamp and provide headroom for jitters
            vbl = Screen('Flip', w, vbl + (waitframes - 0.5) * ifi);
            
            % exit the while loop and flag to one if user presses any key
            if KbCheck
                exitLoop=1;
                break;
            end
        end        
      
        
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % IMPORTANT YOU MUST CLOSE EACH TEXTURE IN THE LOOP OTHERWISE THESE
    % OBJECTS WILL REMAIN IN MEMORY FOR SOME TIME AND ULTIMATELY LEAD TO
    % JAVA OUT OF MEMORY ERRORS!!!
    switch trials(trial).Stimulus_Type
        case 'Full-field Grating'
%             sca
             Screen('Close', gratingTex{trial})
        case 'Masked Grating'
%             sca
             Screen('Close', gratingtex{trial})
             Screen('Close', masktex{trial})           
        case 'Radially Moving Bar'
%             sca
             Screen('Close', barTex{trial}) 
    end
    
 
    %% Send via UDP the trigger to stop the camera:
if LoopCounter == TrialsPerCameraLOOP ;
% Set up UDP connection: port 9090 for EyeTracker PC

udp_InitializeCam=pnet('udpsocket',9090); %Creates a UDP socket and binds it to an UDP port
if udp_InitializeCam<0 % reinitiate udp
    pnet(udp_InitializeCam,'close');
    udp_InitializeCam=pnet('udpsocket',9090);
end

pnet(udp_InitializeCam,'udpconnect','169.230.189.114',9090); %Connect a destination host and port to the the UDP socket
  
message = 'EndTrial' ;   

pnet(udp_InitializeCam,'write',message); % Write message in socket
pnet(udp_InitializeCam,'writepacket'); %Sends contents of the sockets write buffer as a UDP packet 
pnet('closeall');


LoopCounter =0; %Reset the LoopCounter

end

    
        end %(end of trial)
    
    
    
 

%% STOP the camera, after the last trial
% Send via UDP the trigger to stop the camera:

% Set up UDP connection: port 9090 for EyeTracker PC

udp_InitializeCam=pnet('udpsocket',9090); %Creates a UDP socket and binds it to an UDP port
if udp_InitializeCam<0 % reinitiate udp
    pnet(udp_InitializeCam,'close');
    udp_InitializeCam=pnet('udpsocket',9090);
end

pnet(udp_InitializeCam,'udpconnect','169.230.189.114',9090); %Connect a destination host and port to the the UDP socket
  
message = 'EndTrial' ;   

pnet(udp_InitializeCam,'write',message); % Write message in socket
pnet(udp_InitializeCam,'writepacket'); %Sends contents of the sockets write buffer as a UDP packet 
pnet('closeall');
        
        
        
        
        
        
        
        
        
        
        
        
        
        

    
    % Restore normal priority scheduling in case something else was set
    % before:
    Priority(0);
	
	%The same commands wich close onscreen and offscreen windows also close
	%textures. We still need to close any screens opened prior to the trial
	%loop ( the prep screen for example)
	Screen('CloseAll');
    
catch 
    %this "catch" section executes in case of an error in the "try" section
    %above.  Importantly, it closes the onscreen window if its open.
    Screen('CloseAll');
    Priority(0);
    psychrethrow(psychlasterror);
end

%%%%%%%%%%%%%%%%%%%%%%%% Turn On PTB verbose warnings %%%%%%%%%%%%%%%%%%%%
Screen('Preference', 'Verbosity',3);
% please see the following page for an explanation of this function
%  http://psychtoolbox.org/FaqWarningPrefs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
java.lang.Runtime.getRuntime().gc % call garbage collect (likely useless)
return
end


